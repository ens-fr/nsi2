# Accueil

==NOUVEAU== : Préparation à l'épreuve pratique

(Version de **démonstration**) : :+1: :+1: :+1:  <https://nsi-pratique-staging.herokuapp.com/> :+1: :+1: :+1:

> La version publique sera bientôt disponible : https://e-nsi.gitlab.io/nsi-pratique/

Conseil : Commencer par Niveau 1. Chronométrez-vous, et envoyer un bilan des exercices. Dites lesquels sont vos préférés !!!

## Niveau 1

- Dentiste
- Anniversaires
- Compte d'occurrences
- Codage par différence
- Valeurs extrêmes
- Exclamations
- Gelées
- Indice première occurrence
- Collage
- Moyenne pondérée
- Nombre puis double
- Palindrome
- Soleil couchant
- Découpe
- Suite de Syracuse
- Transposition
- Indice et valeur du max

## Niveau 2

- Sortie de labyrinthe
- Puis en partant de la fin !!! (OUI, pas au début !!! Bug...)

---


Auteur : [Franck CHAMBON](https://lyc-84-bollene.gitlab.io/chambon/)

Bienvenue en terminale.

- Relire de très nombreuses fois le cours et bien l'étudier, en particulier ses exercices. On pourra lire d'autres cours, présentés en bas.
- Travailler un maximum sur [France-IOI](http://www.france-ioi.org/) ; objectifs :
    - finir le niveau 3,
    - étudier les [solutions](https://ens-fr.gitlab.io/france-ioi/) de votre professeur,
    - avoir bien entamé le niveau 4 en particulier sur :
        - les arbres ;
        - les balayages de structures de données ;
        - les graphes.
- S'entrainer sur [Prologin](https://prologin.org/).
    - Retrouver les [corrigés](https://ens-fr.gitlab.io/prologin/) de votre professeur ainsi que des propositions d'autres élèves avec des commentaires.

## 🎓Baccalauréat

### Définition de l'épreuve 

[ Note de service n° 2020-030 du 11-2-2020](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm)

1. Partie écrite ; durée : 3 heures 30

2. Partie pratique ; durée : 1 heure

[Exemples de sujets](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/cours/)
> :warning: cette liste d'exercices sera corrigée en janvier 2022 ; il y a des révisions à faire...

### Adaptation

Une partie du programme de NSI ne figurera pas à l'épreuve du baccalauréat. On pourra donc la traiter après les épreuves.

[Note de service du 12-7-2021](https://www.education.gouv.fr/bo/21/Hebdo30/MENE2121274N.htm)

!!! warning "Hors programme BAC"

    À compter de la session 2022 du baccalauréat, les parties du programme de terminale qui ne pourront pas faire l'objet d'une évaluation lors de l'épreuve terminale écrite et pratique de l'enseignement de spécialité numérique et sciences informatiques de la classe de terminale de la voie générale définie dans la note de service n° 2020-030 du 11 février 2020 sont définies comme suit :

    1. Histoire de l'informatique
      
        - Évènements clés de l'histoire de l'informatique
    2. Structures de données
      
        - Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés
    3. Bases de données
      
        - Système de gestion de bases de données relationnelles
    4. Architectures matérielles, systèmes d'exploitation et réseaux
      
        - Sécurisation des communications
    5. Langages et programmation
      
        - Notions de programme en tant que donnée. Calculabilité, décidabilité
        - Paradigmes de programmation
    6. Algorithmique
      
        - Algorithmes sur les graphes
        - Programmation dynamique
        - Recherche textuelle


## 🎲Côté ludique

1. Un jeu : [Py-rates](https://py-rates.fr/index.html)
    - C'est du Python. Facile.

2. Un jeu : [CargoBot](http://www-verimag.imag.fr/~wack/CargoBot/)
    - Vous devez programmer un automate. Beaucoup de niveaux faciles, mais aussi des difficiles.

3. Un jeu : [RoboZZle](http://www.robozzle.com/beta/index.html?puzzle=-1)
    - Vous devez récupérer toutes les étoiles. Quelques niveaux faciles, beaucoup de niveaux qui feront réfléchir.

4. Un jeu : [Cube Composer](https://david-peter.de/cube-composer/)
    - Appliquer des fonctions pour transformer un objet en forme de cubes colorés en un autre. Quelques niveaux faciles, assez vite difficile.

## 📚D'autres cours de NSI

- [Cours de Dominique Filoé](http://siingenieur.free.fr/nsi/partage/reseau/index.html)
- [Cours de Gilles Lassus](https://glassus.github.io/terminale_nsi/)
- [Cours de Fabrice Nativel](https://fabricenativel.github.io/NSITerminale/)
- [Cours de Vincent Bouillot](https://ferney-nsi.gitlab.io/terminale/)
- [Cours de Fred Leleu](https://impytoyable.fr/index.html)
- [Cours du lycée Angellier](https://angellier.gitlab.io/nsi/terminale/)
- [Cours de Vincent-Xavier Jumel](https://lamadone.frama.io/informatique/terminale-nsi/index.html)

