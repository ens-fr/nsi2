# Sommaire

Dans ce chapitre :

- On rappelle qu'un **tableau** est défini avec une taille fixe, avec des éléments de même type. On peut accéder et modifier des éléments.
- On découvre la **pile** en tant que structure abstraite, mais aussi implémentée de plusieurs façons. On peut empiler ou dépiler des éléments, mais on n'a pas d'accès direct aux éléments.
- On découvre la **file**. Comme la **pile**, mais on défile l'élément le plus anciennement enfilé. On rencontrera aussi la deque.
- On découvre la **liste chainée**, simplement ou doublement.
