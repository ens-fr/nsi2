class Deque():
    """Classe deque basée sur les listes dynamiques de Python
    """

    def __init__(self):
        self.données = []

    def est_vide(self):
        return self.données == []

    def ajout_gauche(self, élément):
        self.données = [élément] + self.données
        # ou bien
        #self.données[0:0] = [élément]

    def ajout_droite(self, élément):
        self.données.append(élément)

    def extrait_gauche(self):
        if self.est_vide():
            raise ValueError("Liste vide")
        élément_gauche = self.données.pop(0)
        return élément_gauche

    def extrait_droite(self):
        if self.est_vide():
            raise ValueError("Liste vide")
        élément_droite = self.données.pop()
        return élément_droite

if __name__ == '__main__':
    # tests
    test = Deque()

    # test est_vide
    assert test.est_vide()

    # test un ajout/extraction à droite
    test.ajout_droite(42)
    élément = test.extrait_droite()
    assert élément == 42
    assert test.est_vide()

    # test un ajout/extraction à gauche
    test.ajout_gauche(1337)
    élément = test.extrait_gauche()
    assert élément == 1337
    assert test.est_vide()

    # test plusieurs ajouts
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.ajout_gauche(élément)
    assert [test.extrait_droite()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
    for élément in premiers:
        test.ajout_droite(élément)
    assert [test.extrait_gauche()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
