# Sommaire

Dans ce chapitre, on découvre :

* La notion d'arbre enraciné,
* la notion de tas (hors programme),
* la notion d'arbre binaire de recherche.

