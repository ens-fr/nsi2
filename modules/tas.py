class Tas:
    """ Implémentation de la structure de données tas-max
    """
    def __init__(self):
        self.__tas = [None]
        self.__taille = 0

    def _échange(self, i, j):
        tmp = self.__tas[i]
        self.__tas[i] = self.__tas[j]
        self.__tas[j] = tmp

    def __str__(self):
        return str(self.__tas)

    def est_vide(self):
        return (self.__tas == [None]) and (self.__taille == 0)

    def ajoute(self, x):
        self.__taille += 1
        self.__tas.append(x)
        i = self.__taille
        parent_i = i // 2
        while (i > 1) and self.__tas[parent_i] < self.__tas[i]:
            self._échange(i, parent_i)
            i = parent_i
            parent_i = i // 2

    def extrait(self):

        def est_valide(i):
            """Indique si la règle est respectée pour le nœud stocké en i
            """
            if 2*i > self.__taille:
                # On est sur une feuille
                return True
            if 2*i == self.__taille:
                # Le nœud n'a qu'un enfant à gauche
                return self.__tas[i] >= self.__tas[2*i]
            # Le nœud possède deux enfant, à gauche, à droite
            return     self.__tas[i] >= self.__tas[2*i] \
                   and self.__tas[i] >= self.__tas[2*i + 1]
            
        if self.est_vide():
            raise ValueError("Tas vide")
        if self.__taille == 1:
            self.__taille = 0
            return self.__tas.pop()
        élément = self.__tas[1] # l'élément à renvoyer
        # On place à la racine le dernier élément
        self.__tas[1] = self.__tas.pop()
        self.__taille -= 1
        # On va le remettre à une place qui respecte la règle
        i = 1
        while not est_valide(i):
            if (2*i == self.__taille) \
               or (self.__tas[2*i] > self.__tas[2 * i + 1]):
                    j = 2 * i     # vers l'enfant gauche
            else:
                j = 2 * i + 1 # vers l'enfant droite
            self._échange(i, j)
            i = j
        return élément
        
if __name__ == '__main__':
    # tests
    test = Tas()

    # test est_vide
    assert test.est_vide()

    # test un ajoute/extrait
    univers = 42
    test.ajoute(univers)
    élément = test.extrait()
    assert élément == univers
    assert test.est_vide()

    # test plusieurs ajout/extrait
    #  dans l'ordre
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.ajoute(élément)
    assert [test.extrait()
            for élément in premiers] == premiers[::-1]
    assert test.est_vide()
    
    #  dans le désordre
    from random import shuffle
    bazar = premiers[:]
    for _ in range(100):
        shuffle(bazar)
        for élément in bazar:
            test.ajoute(élément)
        shuffle(bazar)
        assert [test.extrait()
                for élément in bazar] == premiers[::-1]
        assert test.est_vide()
    
